
function displayMsgToSelf(){
	console.log("Don't text him back");
};

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();


// While loop

/* 
	Syntax:
	while(condition){
		statement
	};

*/

let count = 20;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

//While Loop
// Mini Activity
let x = 1;

while(x <= 5){
	console.log(x);
	x++;
};

// Do-While Loop

/* 
	Syntax:
		do {
			statement
		} while (condition)
*/

let doWhileCounter = 1;

do {
	console.log(doWhileCounter)

} while(doWhileCounter === 0)



let number = Number(prompt("Give me a number"));
do {
	console.log("Do While " + number)
	number += 1
} while(number < 10)


// Mini Activity
// Create a d-while loop which will be able

let counter = 1;

do {
	console.log("Do While " + counter)
	counter ++;
	// counter += 1
} while(counter <= 20)

// For Loop
	/*
		for(initialization; condition;
		finalExpression(increment/decrement);){
			statement
		}


	*/

for(let count = 0; count <= 10; count++){
	console.log("For Loop count: " + count);
};

// index starts at 0
// A === [0];
// l === [1];
// E === [2];
// x === [3];
let name = "AlEx";

for(let i = 0; i < name.length; i++){
	if(
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u"
		){
		console.log("3");
	} else {
		console.log(name[i])
	}
};

// Continue and Break

for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){

		continue;
	};

	console.log("Continue and Break " + count);

	if(count > 10){
		break;
	};
};

let myName = "alexandro";

for(let i = 0; i < myName.length; i++) {

	console.log(myName[i]);

	if(myName[i].toLowerCase() === "a"){
	console.log("Continue to next iteration")

	continue; // ignored all the remaining codes below once condition met, then will continue to the next iteration
}
	if(myName[i].toLowerCase() === "d"){
		break; //get out of the loops once condition met
	};
};
